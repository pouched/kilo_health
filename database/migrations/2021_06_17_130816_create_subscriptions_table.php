<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->id();
            $table->integer('customer_service_id');
            $table->string('payment-service');
            $table->string('payment-service-subscription-id');
            $table->boolean('renew-status');
            $table->timestamp('expires-date');
            $table->timestamps();

            $table->foreign('customer_service_id')
                ->references('id')
                ->on('customer_services')
                ->restrictOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
