<?php

namespace App\Providers;

use App\Events\SubscriptionCancel;
use App\Events\SubscriptionFail;
use App\Events\SubscriptionInit;
use App\Events\SubscriptionRenew;
use App\Listeners\SubscriptionCancelHandler;
use App\Listeners\SubscriptionFailHandler;
use App\Listeners\SubscriptionInitHandler;
use App\Listeners\SubscriptionRenewHandler;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        SubscriptionInit::class => [
            SubscriptionInitHandler::class,
        ],
        SubscriptionRenew::class => [
            SubscriptionRenewHandler::class,
        ],
        SubscriptionCancel::class => [
            SubscriptionCancelHandler::class
        ],
        SubscriptionFail::class => [
            SubscriptionFailHandler::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
