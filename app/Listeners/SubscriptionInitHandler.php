<?php

namespace App\Listeners;

use App\Events\SubscriptionInit;
use App\Models\CustomerService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SubscriptionInitHandler
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SubscriptionInit  $event
     * @return void
     */
    public function handle(SubscriptionInit $event)
    {
        $data = $event->data;

        $customerService = CustomerService::findOrFail($data['customerServiceId']);

        $subscription = $customerService->subscriptions->create([
            'payment-service' => $data['paymentService'],
            'payment-service-subscription-id' => $data['paymentServiceSubscriptionId'],
            'renew-status' => $data['renewStatus'],
            'expires-date' => $data['expiresDate'],
        ]);

        $subscription->transactions->create([
            'paymentServiceTransactionId' => $data['paymentServiceTransactionId'],
            'status' => 'success'
        ]);
    }
}
