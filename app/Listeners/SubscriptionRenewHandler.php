<?php

namespace App\Listeners;

use App\Events\SubscriptionRenew;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SubscriptionRenewHandler
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SubscriptionRenew  $event
     * @return void
     */
    public function handle(SubscriptionRenew $event)
    {
        //
    }
}
