<?php

namespace App\Listeners;

use App\Events\SubscriptionCancel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SubscriptionCancelHandler
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SubscriptionCancel  $event
     * @return void
     */
    public function handle(SubscriptionCancel $event)
    {
        //
    }
}
