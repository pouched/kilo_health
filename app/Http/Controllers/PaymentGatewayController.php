<?php

namespace App\Http\Controllers;

use App\Support\Payment\Contracts\PaymentGatewayContract;
use Illuminate\Http\Request;

class PaymentGatewayController extends Controller
{
    /**
     * @var PaymentGatewayContract
     */
    private $paymentGateway;

    public function __construct(PaymentGatewayContract $paymentGateway) {
        $this->paymentGateway = $paymentGateway;
    }

    /**
     * @param Request $request
     */
    public function paymentWebhook(Request $request) {
        try {
            $this->paymentGateway->processWebhook($request);
        } catch (\Exception $exception) {
            logger()->error($exception->getMessage(), ['exception' => $exception]);
            return ['success' => false];
        }
        return ['success' => true];
    }

}
