<?php


namespace App\Support\Payment;

use App\Events\SubscriptionFail;
use App\Events\SubscriptionInit;
use App\Events\SubscriptionRenew;
use App\Exceptions\PaymentException;
use App\Support\Payment\Contracts\PaymentServiceContract;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\ParameterBag;

class ApplePayPaymentService implements PaymentServiceContract
{
    protected static string $paymentService = 'apple-pay';

    protected static array $handlers = [
        'INITIAL_BUY' => 'subscriptionInit',
        'DID_RENEW' => 'subscriptionRenew',
        'DID_FAIL_TO_RENEW' => 'subscriptionFail',
        'CANCEL' => 'subscriptionCancel'
    ];

    /**
     * @param Request $request
     * @throws PaymentException
     */
    public function processWebhook(Request $request)
    {
        $this->authenticateRequest($request);

        $appleEventType = $request->json()->get('notification_type');

        if (isset(self::$handlers[$appleEventType])) {
            $this->{self::$handlers[$appleEventType]}($request->json());
        }
    }

    /**
     * @param Request $request
     */
    private function authenticateRequest(Request $request)
    {
        $password = $request->json()->get('password');
        // TODO figure out how to validate a password
        // as I understand some date should be send by front app?

        if (false) {
            logger()->error('can\'t verify payment information', ['json' => $request->json()->all()]);
            throw new PaymentException('payment authentication failed');
        }
    }

    /**
     * @param ParameterBag $json
     * @throws PaymentException
     */
    private function subscriptionInit(ParameterBag $json) {
        $renewStatus = $json->get('auto_renew_status');

        $latestTransactions = $json->get('unified_receipt')['latest_receipt_info'];

        $lastTransactionIndex = 0;
        $latestTransaction = $latestTransactions[$lastTransactionIndex];

        $customerServiceId = $latestTransaction['product_id'];
        $appleSubscriptionId = $latestTransaction['original_transaction_id'];
        $appleTransactionId = $latestTransaction['transaction_id'];
        $expiresDate = $latestTransaction['expires_date'];

        SubscriptionInit::dispatch([
            'paymentService' => self::$paymentService,
            'customerServiceId' => $customerServiceId,
            'paymentServiceSubscriptionId' => $appleSubscriptionId,
            'paymentServiceTransactionId' => $appleTransactionId,
            'renewStatus' => $renewStatus,
            'expiresDate' => $expiresDate
        ]);
    }

    /**
     * @param ParameterBag $json
     */
    private function subscriptionRenew(ParameterBag $json) {
        $renewStatus = $json->get('auto_renew_status');

        $latestTransactions = $json->get('unified_receipt')['latest_receipt_info'];

        $lastTransactionIndex = 0;
        $latestTransaction = $latestTransactions[$lastTransactionIndex];

        $customerServiceId = $latestTransaction['product_id'];
        $appleSubscriptionId = $latestTransaction['original_transaction_id'];
        $appleTransactionId = $latestTransaction['transaction_id'];
        $expiresDate = $latestTransaction['expires_date'];

        SubscriptionRenew::dispatch([
            'paymentService' => self::$paymentService,
            'customerServiceId' => $customerServiceId,
            'paymentServiceSubscriptionId' => $appleSubscriptionId,
            'paymentServiceTransactionId' => $appleTransactionId,
            'renewStatus' => $renewStatus,
            'expiresDate' => $expiresDate
        ]);
    }

    private function subscriptionFail(ParameterBag $json) {
        $renewStatus = $json->get('auto_renew_status');

        $latestTransactions = $json->get('unified_receipt')['latest_receipt_info'];

        $lastTransactionIndex = 0;
        $latestTransaction = $latestTransactions[$lastTransactionIndex];

        $customerServiceId = $latestTransaction['product_id'];
        $appleSubscriptionId = $latestTransaction['original_transaction_id'];
        $appleTransactionId = $latestTransaction['transaction_id'];
        $expiresDate = $latestTransaction['expires_date'];

        SubscriptionFail::dispatch([
            'paymentService' => self::$paymentService,
            'customerServiceId' => $customerServiceId,
            'paymentServiceSubscriptionId' => $appleSubscriptionId,
            'paymentServiceTransactionId' => $appleTransactionId,
            'renewStatus' => $renewStatus,
            'expiresDate' => $expiresDate
        ]);
    }

    private function subscriptionCancel(ParameterBag $json) {
        $renewStatus = $json->get('auto_renew_status');

        $latestTransactions = $json->get('unified_receipt')['latest_receipt_info'];

        $lastTransactionIndex = 0;
        $latestTransaction = $latestTransactions[$lastTransactionIndex];

        $customerServiceId = $latestTransaction['product_id'];

        SubscriptionFail::dispatch([
            'paymentService' => self::$paymentService,
            'customerServiceId' => $customerServiceId,
            'renewStatus' => $renewStatus,
        ]);
    }
}
