<?php


namespace App\Support\Payment\Contracts;

use App\Models\Transaction;
use Illuminate\Http\Request;

interface PaymentServiceContract
{
    public function processWebhook(Request $request);
}

