<?php

namespace App\Support\Payment\Contracts;

use Illuminate\Http\Request;

interface PaymentGatewayContract
{
    /**
     * @param $request
     * @return mixed
     */
    public function processWebhook(Request $request);//todo make a PSR interface implementation
}
