<?php

namespace App\Support\Payment;

use App\Exceptions\PaymentException;
use App\Support\Payment\Contracts\PaymentGatewayContract;
use App\Support\Payment\Contracts\PaymentServiceContract;
use Illuminate\Http\Request;

class PaymentGateway implements PaymentGatewayContract
{
    public function __construct() {}

    public function processWebhook(Request $request)
    {
        $paymentServiceName = $request->get('paymentService');

        $paymentServiceClass = $this->getPaymentServiceClass($paymentServiceName);

        if (
            !class_exists($paymentServiceClass)
            || !is_a($paymentServiceClass, PaymentServiceContract::class, true)) {
            throw new PaymentException('Payment method is not supported');
        }

        app()->make($paymentServiceClass)->processWebhook($request);
    }

    /**
     * @param $paymentServiceName
     * @return string
     */
    protected function getPaymentServiceClass($paymentServiceName): string
    {
        $paymentServiceClass = trim(
            str_replace(' ', '',
                ucwords(
                    str_replace('-', ' ', $paymentServiceName)
                )
            )
        );

        $paymentServiceClass = 'App\\Support\\Payment\\' . $paymentServiceClass . 'PaymentService';

        return $paymentServiceClass;
    }
}
