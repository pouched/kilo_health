<?php

namespace Tests\Feature\Payment;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ApplePaymentTest extends TestCase
{
    /**
     * @test
     */
    public function itShouldInitNewSubscription()
    {
        $response = $this->postJson('/', [
            'auto_renew_adam_id' => '2323423423232',
            'auto_renew_product_id' => '11222333444',
            'auto_renew_status' => true,
            'notification_type' => 'INITIAL_BUY',
            'password' => 'big_secret',
            'unified_receipt' => [
                [
                    'environment' => 'Sandbox',
                    'cancellation_date' => '',//ISO 8601
                    'expires_date' => time() + 10000,

                ]
            ]
        ]);

        $response->assertStatus(200);
    }
}
